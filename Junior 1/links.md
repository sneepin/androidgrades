1. Activity stack  
  - https://habrahabr.ru/post/186434/  
  - https://inthecheesefactory.com/blog/understand-android-activity-launchmode/en
2. Broadcast
  - http://codetheory.in/android-broadcast-receivers/
  - http://stackoverflow.com/questions/2808796/what-is-an-android-pendingintent/15873786#15873786
3. Preference
  - https://developer.android.com/guide/topics/ui/settings.html
4. Percent layout
  - https://inthecheesefactory.com/blog/know-percent-support-library/en
5. Context
  - https://possiblemobile.com/2013/06/context/
6. Multiple screen support
  - https://www.captechconsulting.com/blogs/understanding-density-independence-in-android
  - https://developer.android.com/guide/practices/screens_support.html
7. Fragments
  - https://www.raywenderlich.com/117838/introduction-to-android-fragments-tutorial
  - https://github.com/xxv/android-lifecycle
8. Resource qualifiers
  - https://developer.android.com/guide/topics/resources/providing-resources.html
9. Dialogs
  - https://github.com/afollestad/material-dialogs
  - https://developer.android.com/guide/topics/ui/dialogs.html
10. Processes
  - https://developer.android.com/guide/components/processes-and-threads.html
11. Screen orientation
  - http://stackoverflow.com/questions/18268218/change-screen-orientation-programatically-using-a-button
12. Intent
  - https://developer.android.com/guide/components/intents-filters.html
  - https://developer.android.com/reference/android/content/Intent.html
  - http://stackoverflow.com/questions/525063/android-respond-to-url-in-intent
  - http://pscout.csl.toronto.edu/downloads.php
