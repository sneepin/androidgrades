1. fonts
  - http://stackoverflow.com/questions/2888508/how-to-change-the-font-on-the-textview
2. Styles and themes
  - https://developer.android.com/guide/topics/ui/themes.html
  - http://stackoverflow.com/questions/11723881/android-set-view-style-programatically
3. Archives
  - http://stackoverflow.com/questions/3382996/how-to-unzip-files-programmatically-in-android
  - http://stackoverflow.com/questions/315618/how-do-i-extract-a-tar-file-in-java/7556307#7556307
4. Tabs
  - http://www.androidhive.info/2015/09/android-material-design-working-with-tabs/
  - bottom navigation https://github.com/aurelhubert/ahbottomnavigation
  - dynamic add/delete http://stackoverflow.com/questions/34306476/dynamically-add-and-remove-tabs-in-tablayoutmaterial-design-android
5. Clipboarding
  - https://developer.android.com/guide/topics/text/copy-paste.html#Pasting
6. Notification
  - https://developer.android.com/guide/topics/ui/notifiers/notifications.html
7. Event bus
  - http://www.andreas-schrade.de/2015/11/28/android-how-to-use-the-greenrobot-eventbus/
  - http://endlesswhileloop.com/blog/2015/06/11/stop-using-event-buses/
  - https://medium.com/@diolor/rxjava-as-event-bus-the-right-way-10a36bdd49ba
  - http://nerds.weddingpartyapp.com/tech/2014/12/24/implementing-an-event-bus-with-rxjava-rxbus/
8. Picasso/Glide
  - picasso https://futurestud.io/blog/picasso-adapter-use-for-listview-gridview-etc
  - glide  https://futurestud.io/blog/glide-getting-started
9. Processes
  - https://medium.com/@rotxed/going-multiprocess-on-android-52975ed8863c#.g4vf4cqo2
10. Push
  - https://www.digitalocean.com/community/tutorials/how-to-create-a-server-to-send-push-notifications-with-gcm-to-android-devices-using-python
  - https://developers.google.com/instance-id/guides/android-implementation
  - https://developers.google.com/cloud-messaging/android/client
