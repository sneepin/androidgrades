1. Support library
   - http://martiancraft.com/blog/2015/06/android-support-library/
2. Manifest
  - https://developer.android.com/guide/topics/manifest/manifest-intro.html
  - http://stackoverflow.com/questions/8816623/how-to-use-custom-permissions-in-android
  - https://www.owasp.org/images/c/ca/ASDC12-An_InDepth_Introduction_to_the_Android_Permissions_Modeland_How_to_Secure_MultiComponent_Applications.pdf
3. Files
  - http://stackoverflow.com/questions/23205389/using-getexternalfilesdir-with-multi-sdcards-galaxy-s3
4. Toolbar/actionbar
  - https://guides.codepath.com/android/Extended-ActionBar-Guide
5. SearchView
  - https://www.youtube.com/watch?v=9OWmnYPX1uc
  - https://philio.me/styling-the-searchview-with-appcompat-v21/
6. Loaders
  - http://www.grokkingandroid.com/using-loaders-in-android/
  - http://www.androiddesignpatterns.com/2012/07/loaders-and-loadermanager-background.html
7. WebView
  - http://tutorials.jenkov.com/android/android-web-apps-using-android-webview.html#caching-web-resources-in-the-android-device
  - http://stackoverflow.com/questions/2835556/whats-the-difference-between-setwebviewclient-vs-setwebchromeclient
8. Custom view
  - https://github.com/codepath/android_guides/wiki/Defining-Custom-Views
