package com.github.sneepin.loaders;

import com.google.gson.annotations.SerializedName;

/**
 * @author ravil
 */
public class Airport {

    @SerializedName("iata")
    private String mIata;

    @SerializedName("name")
    private String mName;

    @SerializedName("airport_name")
    private String mAirportName;

    public Airport() {
    }
}
