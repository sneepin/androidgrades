package com.github.sneepin.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.io.IOException;
import java.util.List;

/**
 * @author ravil
 */
public class AirportsListLoader extends AsyncTaskLoader<List<Airport>> {

    private AirportsService mAirportsService;

    public AirportsListLoader(Context context) {
        super(context);

        mAirportsService = ApiFactory.getAirportsService();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        forceLoad();
    }

    @Override
    public List<Airport> loadInBackground() {
        try {
            return mAirportsService.airports("55.749792,37.6324949").execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
