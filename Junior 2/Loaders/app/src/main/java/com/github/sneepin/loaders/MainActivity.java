package com.github.sneepin.loaders;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.List;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Airport>> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getLoaderManager().initLoader(0, Bundle.EMPTY, this);
    }


    @Override
    public Loader<List<Airport>> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case 0:
                return new AirportsListLoader(this);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<List<Airport>> loader, List<Airport> data) {
        System.out.println(data.size());
    }

    @Override
    public void onLoaderReset(Loader<List<Airport>> loader) {

    }
}
