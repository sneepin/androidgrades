package com.github.sneepin.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author ravil
 */
public class ShapeSelectorView extends View {

    private int mColor;
    private boolean mIsDisplayName;
    private int shapeWidth = 100;
    private int shapeHeight = 100;
    private int textXOffset = 0;
    private int textYOffset = 30;
    private Paint paintShape;


    public ShapeSelectorView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setupAttrs(attrs);

        paintShape = new Paint();
        paintShape.setStyle(Paint.Style.FILL);
        paintShape.setColor(mColor);
        paintShape.setTextSize(30);
    }

    private void setupAttrs(AttributeSet attributeSet) {
        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.ShapeSelectorView, 0, 0);

        mColor = typedArray.getColor(R.styleable.ShapeSelectorView_shapeColor, Color.BLACK);
        mIsDisplayName = typedArray.getBoolean(R.styleable.ShapeSelectorView_displayShapeName, false);

        typedArray.recycle();
    }

    public int getColor() {
        return mColor;
    }

    public void setColor(int color) {
        mColor = color;

        invalidate();
        requestLayout();
    }

    public boolean isDisplayName() {
        return mIsDisplayName;
    }

    public void setIsDisplayName(boolean isDisplayName) {
        mIsDisplayName = isDisplayName;

        invalidate();
        requestLayout();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRect(0, 0, shapeWidth, shapeHeight, paintShape);
        if (mIsDisplayName) {
            canvas.drawText("Square", shapeWidth + textXOffset, shapeHeight + textXOffset, paintShape);
        }

    }
}
