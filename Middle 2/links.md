1. Https
  - https://ru.wikipedia.org/wiki/HTTPS
2. Telephony manager
  - https://en.wikipedia.org/wiki/Mobile_country_code
3. Deep links
  - http://nerds.airbnb.com/deeplinkdispatch/
  - https://github.com/airbnb/DeepLinkDispatch
  - http://search-codelabs.appspot.com/codelabs/android-deep-linking#8
4. Паттерны
5. Touch/Multitouch/TouchIntercept
  - http://pierrchen.blogspot.ru/2014/03/pipeline-of-android-touch-event-handling.html
  - http://codetheory.in/understanding-android-input-touch-events/
  - multitouch https://developer.android.com/training/gestures/multi.html
6. Gestures
  - https://github.com/jasonpolites/gesture-imageview
  - http://codetheory.in/android-gesturedetector/
  - https://developer.android.com/training/gestures/detector.html
  - http://stackoverflow.com/questions/28098737/difference-between-onscroll-and-fling
7. Animation
  - https://developer.android.com/guide/topics/graphics/prop-animation.html
  - https://github.com/lgvalle/Material-Animations
  - https://medium.com/@andkulikov/animate-all-the-things-transitions-in-android-914af5477d50#.lkveg63ve
  - https://github.com/wasabeef/recyclerview-animators
8. User location
  - google service https://developer.android.com/training/location/retrieve-current.html
  - location api https://developer.android.com/guide/topics/location/strategies.html
9. Week Reference
  - https://habrahabr.ru/post/169883/
10. Drag and drop
  - https://medium.com/@ipaulpro/drag-and-swipe-with-recyclerview-b9456d2b1aaf#.5mrxtlohz
  - https://developer.android.com/guide/topics/ui/drag-drop.html
