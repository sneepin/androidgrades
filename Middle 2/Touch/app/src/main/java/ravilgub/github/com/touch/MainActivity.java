package ravilgub.github.com.touch;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "view touch";

    private ViewGroup mRoot;

    private View mView1;
    private View mView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRoot = (ViewGroup) findViewById(R.id.root_view);
        mView1 = findViewById(R.id.view1);
        mView2 = findViewById(R.id.view2);

        mView2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, event.getAction() + " view 2 and pinter id " + event.getPointerId(0));



                return false;
            }
        });

        mView1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "on touch on view 1" + event.getPointerId(0));

                return false;
            }
        });

        mRoot.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "on touch on root view with pointer id" + event.getPointerId(0));

                return true;
            }
        });
    }


}
