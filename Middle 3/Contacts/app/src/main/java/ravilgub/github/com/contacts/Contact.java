package ravilgub.github.com.contacts;

import io.realm.RealmObject;

/**
 * @author ravil
 */
public class Contact extends RealmObject {

    private String mName;

    public Contact() {
    }

    public Contact(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}

