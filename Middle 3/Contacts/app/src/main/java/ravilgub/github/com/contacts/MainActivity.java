package ravilgub.github.com.contacts;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity {

    private Realm mRealm;

    private Button mRestoreButton;

    private Button mSaveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRestoreButton = (Button) findViewById(R.id.btn_restore);
        mRestoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restore();
            }
        });

        mSaveButton = (Button) findViewById(R.id.btn_save);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveContancts();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(realmConfig);
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRealm.close();
    }

    private void saveContancts() {
        mRealm.beginTransaction();
        mRealm.delete(Contact.class);
        mRealm.commitTransaction();

        ContentResolver resolver = getContentResolver();

        Cursor cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, null ,null, null);

        if (cursor != null) {
            int size = cursor.getCount();

            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                Contact contact = new Contact(name);
                mRealm.beginTransaction();
                mRealm.copyToRealm(contact);
                mRealm.commitTransaction();
            }
        }
        if (cursor != null) {
            cursor.close();
        }
    }

    private void restore() {
        List<Contact> contactList = mRealm.where(Contact.class).findAll();

        ContentValues contentValues = new ContentValues();

        contentValues.put(ContactsContract.RawContacts.CONTACT_ID, "NEW CONTECT");
        contentValues.put(ContactsContract.Contacts.DISPLAY_NAME, "NEW CONTECT");
        getContentResolver().insert(android.provider.ContactsContract.RawContacts.CONTENT_URI, contentValues);

        for (Contact contact : contactList) {

            ContentValues values = new ContentValues();
            values.put(ContactsContract.RawContacts.ACCOUNT_TYPE, "my type");
            values.put(ContactsContract.RawContacts.ACCOUNT_NAME, "new my account");
            Uri rawContactUri = getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, values);
            long rawContactId = ContentUris.parseId(rawContactUri);

            values.clear();
            values.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
            values.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
            values.put(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contact.getName());
            getContentResolver().insert(ContactsContract.Data.CONTENT_URI, values);

        }

    }
}
