1. Data Binding
  - http://androidgig.com/two-way-databinding-in-android/
2. QR/Barcode code reader
  - QR code https://github.com/zxing/zxing
  - https://github.com/journeyapps/zxing-android-embedded
3. Content provider
  - http://www.grokkingandroid.com/android-tutorial-writing-your-own-content-provider/
  - http://www.grokkingandroid.com/android-tutorial-content-provider-basics/
  - http://www.grokkingandroid.com/android-tutorial-using-content-providers/
4. Contacts
  - https://developer.android.com/reference/android/provider/ContactsContract.html
  - http://stackoverflow.com/questions/12562151/android-get-all-contacts
  - http://stackoverflow.com/questions/16414351/programmatically-inserting-new-android-contacts-into-phone
5. SMS
  - https://developer.android.com/reference/android/telephony/SmsManager.html
  - http://codetheory.in/android-sms/
  - http://stackoverflow.com/questions/8578689/sending-text-messages-programmatically-in-android
6. Account manager
  - http://blog.udinic.com/2013/04/24/write-your-own-android-authenticator/
  - https://developer.android.com/reference/android/accounts/AccountManager.html
8. Web sockets
 - https://github.com/TooTallNate/Java-WebSocket
 - https://habrahabr.ru/post/235585/
 - https://habrahabr.ru/post/79038/
 9. Robolectric
 
