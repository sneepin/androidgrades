1. RxJava
  - http://tomstechnicalblog.blogspot.ru/2015/10/understanding-observable-emissions.html
  - http://tomstechnicalblog.blogspot.ru/2016/02/rxjava-understanding-observeon-and.html
  - http://rxmarbles.com/
  - http://habrahabr.ru/post/265997/
  - http://habrahabr.ru/post/267243/
  - http://habrahabr.ru/company/e-Legion/blog/272459/
  - coursera "Reactive programming"
2. Design support library
  - http://saulmm.github.io/mastering-coordinator
3. Структуры
  - https://habrahabr.ru/post/237043/
  - https://developer.android.com/reference/android/util/SparseArray.html
  - http://beginnersbook.com/2014/06/difference-between-iterator-and-listiterator-in-java/
4. Retrofit
  - https://futurestud.io/blog/retrofit-getting-started-and-android-client
  - https://github.com/square/okhttp/wiki/Recipes
5. Handler, looper
  - https://blog.nikitaog.me/2014/10/11/android-looper-handler-handlerthread-i/
  - http://baptiste-wicht.com/posts/2010/09/java-concurrency-part-7-executors-and-thread-pools.html
  - http://codetheory.in/android-handlers-runnables-loopers-messagequeue-handlerthread/
6. DatePicker
  - https://github.com/wdullaer/MaterialDateTimePicker
7. SQLite
  - http://www.androidhive.info/2011/11/android-sqlite-database-tutorial/
  - http://stackoverflow.com/questions/4957009/how-do-i-join-two-sqlite-tables-in-my-android-application
8. Service, IntentService
  - http://techtej.blogspot.ru/2011/03/android-thread-constructspart-4.html
  - http://stackoverflow.com/questions/15524280/service-vs-intentservice
  - https://developer.android.com/guide/components/services.html
9. Permissions
  - https://inthecheesefactory.com/blog/things-you-need-to-know-about-android-m-permission-developer-edition/en
  - https://github.com/googlesamples/android-RuntimePermissions
  - https://github.com/hotchemi/PermissionsDispatcher
