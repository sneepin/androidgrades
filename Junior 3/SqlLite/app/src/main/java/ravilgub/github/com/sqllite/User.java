package ravilgub.github.com.sqllite;

import android.provider.BaseColumns;

/**
 * @author ravil
 */
public class User {

    private String mUsername;

    private String mPassword;

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public static class UserContract implements BaseColumns {

        public static final String TABLE_NAME = "users";

        public static final String USERNAME_COLUMN = "username";

        public static final String PASSWORD_COLUMN = "password";
    }
}
