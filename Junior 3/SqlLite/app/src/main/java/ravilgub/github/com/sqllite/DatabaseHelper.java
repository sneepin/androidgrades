package ravilgub.github.com.sqllite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ravilgub.github.com.sqllite.User.UserContract;

/**
 * @author ravil
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "TestDB";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createUserTableQuery = "CREATE TABLE " + UserContract.TABLE_NAME + " ("
                + UserContract._ID + " INTEGER PRIMARY KEY," + UserContract.USERNAME_COLUMN + " TEXT,"
                + UserContract.PASSWORD_COLUMN + " TEXT" + " )";

        db.execSQL(createUserTableQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + UserContract.TABLE_NAME);

        onCreate(db);
    }

    public void addUser(@NonNull User user) {
        SQLiteDatabase database = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(UserContract.USERNAME_COLUMN, user.getUsername());
        contentValues.put(UserContract.PASSWORD_COLUMN, user.getPassword());

        database.insert(UserContract.TABLE_NAME, null, contentValues);
        database.close();
    }

    @NonNull
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();

        String query = "SELECT * FROM " + UserContract.TABLE_NAME;

        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setUsername(cursor.getString(1));
                user.setPassword(cursor.getString(2));

                users.add(user);
            } while (cursor.moveToNext());
        }

        return users;
    }
}
